package cl.psepulvedao.betterfly_pablosepulveda.data.remote

import cl.psepulvedao.betterfly_pablosepulveda.data.remote.api.CharacterApi
import cl.psepulvedao.betterfly_pablosepulveda.data.remote.mapper.toModelCharacter
import cl.psepulvedao.betterfly_pablosepulveda.data.remote.mapper.toModelCharacterList
import cl.psepulvedao.betterfly_pablosepulveda.data.remote.mapper.toModelInfo
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.Character
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.CharacterList
import cl.psepulvedao.betterfly_pablosepulveda.domain.repository.CharacterRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class RetrofitCharacterRepository: CharacterRepository, KoinComponent {

    private val api: CharacterApi by inject()

    override fun getAll(page: Int?): Flow<CharacterList> = flow {
        try {
            val params = page?.let { mapOf(
                "page" to "$it"
            ) } ?: emptyMap()

            val response = api.getCharacterList(params)

            if(response.isSuccessful) {
                val result = response.body() ?: error("")
                emit(
                    CharacterList(
                        info = result.info.toModelInfo(),
                        characters = result.results.toModelCharacterList()
                    )
                )
            } else {
                error("")
            }
        } catch (ex: Exception) {
            error("")
        }
    }

    override fun getDetail(id: Int): Flow<Character> = flow {
        try {
            val response = api.getCharacterDetail(id)

            if(response.isSuccessful) {
                val result = response.body() ?: error("")
                emit(result.toModelCharacter())
            } else {
                error("")
            }
        } catch (ex: Exception) {
            error("")
        }
    }
}