package cl.psepulvedao.betterfly_pablosepulveda.data.di

import cl.psepulvedao.betterfly_pablosepulveda.data.firebase.FirebaseConfigRepository
import cl.psepulvedao.betterfly_pablosepulveda.data.remote.RetrofitCharacterRepository
import cl.psepulvedao.betterfly_pablosepulveda.domain.repository.CharacterRepository
import cl.psepulvedao.betterfly_pablosepulveda.domain.repository.ConfigRepository
import org.koin.core.component.KoinApiExtension
import org.koin.dsl.module

@OptIn(KoinApiExtension::class)
val repositoryModule = module {
    factory<CharacterRepository> { RetrofitCharacterRepository() }
    single<ConfigRepository> { FirebaseConfigRepository() }
}