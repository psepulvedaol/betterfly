package cl.psepulvedao.betterfly_pablosepulveda.data.remote.response

import cl.psepulvedao.betterfly_pablosepulveda.data.remote.scheme.CharacterScheme
import cl.psepulvedao.betterfly_pablosepulveda.data.remote.scheme.InfoScheme
import com.google.gson.annotations.SerializedName

data class GetCharacterListResponse(
    @SerializedName("info") val info: InfoScheme,
    @SerializedName("results") val results: List<CharacterScheme>
)