package cl.psepulvedao.betterfly_pablosepulveda.data.remote.mapper

import cl.psepulvedao.betterfly_pablosepulveda.data.remote.scheme.InfoScheme
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.Info

fun InfoScheme.toModelInfo() = Info(
    count = count,
    pages = pages
)