package cl.psepulvedao.betterfly_pablosepulveda.data.remote.mapper

import cl.psepulvedao.betterfly_pablosepulveda.data.remote.scheme.CharacterScheme
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.Character

fun CharacterScheme.toModelCharacter() = Character(
    id = id,
    name = name,
    status = status,
    species = species,
    type = type,
    gender = gender,
    image = image
)

fun List<CharacterScheme>.toModelCharacterList() = mutableListOf<Character>().apply {
    this@toModelCharacterList.forEach { add(it.toModelCharacter()) }
}