package cl.psepulvedao.betterfly_pablosepulveda.data.firebase

import cl.psepulvedao.betterfly_pablosepulveda.domain.model.HomeConfiguration
import cl.psepulvedao.betterfly_pablosepulveda.domain.repository.ConfigRepository
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent

@KoinApiExtension
class FirebaseConfigRepository: ConfigRepository, KoinComponent {

    private val config: FirebaseRemoteConfig = FirebaseRemoteConfig.getInstance()

    override fun getHomeConfiguration() = HomeConfiguration(
        isDetailEnabled = config.getBoolean("is_detail_enabled")
    )

}