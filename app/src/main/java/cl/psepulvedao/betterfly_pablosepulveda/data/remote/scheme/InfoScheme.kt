package cl.psepulvedao.betterfly_pablosepulveda.data.remote.scheme

import com.google.gson.annotations.SerializedName

data class InfoScheme(
    @SerializedName("count") val count: Int,
    @SerializedName("pages") val pages: Int,
    @SerializedName("next") val next: String?,
    @SerializedName("prev") val prev: String?
)