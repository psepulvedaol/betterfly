package cl.psepulvedao.betterfly_pablosepulveda.data.di

import cl.psepulvedao.betterfly_pablosepulveda.data.remote.api.CharacterApi
import org.koin.dsl.module
import retrofit2.Retrofit

val apiModule = module {
    single { get<Retrofit>().create(CharacterApi::class.java) }
}