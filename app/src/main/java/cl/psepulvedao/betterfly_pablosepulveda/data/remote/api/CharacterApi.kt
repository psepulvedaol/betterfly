package cl.psepulvedao.betterfly_pablosepulveda.data.remote.api

import cl.psepulvedao.betterfly_pablosepulveda.data.remote.response.GetCharacterListResponse
import cl.psepulvedao.betterfly_pablosepulveda.data.remote.scheme.CharacterScheme
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface CharacterApi {

    @GET("character")
    suspend fun getCharacterList( @QueryMap params: Map<String, String>): Response<GetCharacterListResponse>

    @GET("character/{characterId}")
    suspend fun getCharacterDetail(@Path("characterId") characterId: Int): Response<CharacterScheme>

}