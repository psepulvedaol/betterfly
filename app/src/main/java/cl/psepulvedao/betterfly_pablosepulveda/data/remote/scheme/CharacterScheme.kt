package cl.psepulvedao.betterfly_pablosepulveda.data.remote.scheme

import com.google.gson.annotations.SerializedName

data class CharacterScheme(
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("status") val status: String,
    @SerializedName("species") val species: String,
    @SerializedName("type") val type: String,
    @SerializedName("gender") val gender: String,
    @SerializedName("image") val image: String
)