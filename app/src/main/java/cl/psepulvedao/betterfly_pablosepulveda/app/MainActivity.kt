package cl.psepulvedao.betterfly_pablosepulveda.app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cl.psepulvedao.betterfly_pablosepulveda.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}