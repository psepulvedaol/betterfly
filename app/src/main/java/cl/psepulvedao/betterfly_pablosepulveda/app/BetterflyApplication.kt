package cl.psepulvedao.betterfly_pablosepulveda.app

import android.app.Application
import cl.psepulvedao.betterfly_pablosepulveda.R
import cl.psepulvedao.betterfly_pablosepulveda.data.di.apiModule
import cl.psepulvedao.betterfly_pablosepulveda.data.di.repositoryModule
import cl.psepulvedao.betterfly_pablosepulveda.data.di.retrofitModule
import cl.psepulvedao.betterfly_pablosepulveda.domain.di.useCaseModule
import cl.psepulvedao.betterfly_pablosepulveda.presentation.di.viewModelModule
import com.google.firebase.FirebaseApp
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BetterflyApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        initRemoteConfig()
        initKoin()
    }

    private fun initKoin() = startKoin {
        androidContext(this@BetterflyApplication)
        modules(
            retrofitModule,
            apiModule,
            repositoryModule,
            useCaseModule,
            viewModelModule
        )
    }

    private fun initRemoteConfig() = with(Firebase.remoteConfig) {
        val configSettings = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 10 // Tiempo absurdamente bajo para pruebas
        }
        setConfigSettingsAsync(configSettings)
        setDefaultsAsync(R.xml.remote_config_defaults)
        fetchAndActivate()
    }
}