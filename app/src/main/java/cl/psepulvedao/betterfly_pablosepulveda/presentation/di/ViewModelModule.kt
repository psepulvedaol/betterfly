package cl.psepulvedao.betterfly_pablosepulveda.presentation.di

import cl.psepulvedao.betterfly_pablosepulveda.presentation.detail.DetailViewModel
import cl.psepulvedao.betterfly_pablosepulveda.presentation.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.component.KoinApiExtension
import org.koin.dsl.module

@OptIn(KoinApiExtension::class)
val viewModelModule = module {
    viewModel { HomeViewModel() }
    viewModel { DetailViewModel() }
}