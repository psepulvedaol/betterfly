package cl.psepulvedao.betterfly_pablosepulveda.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.psepulvedao.betterfly_pablosepulveda.domain.usecase.GetCharacterListUseCase
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.Character
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.HomeConfiguration
import cl.psepulvedao.betterfly_pablosepulveda.domain.usecase.GetHomeConfigurationUseCase
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class HomeViewModel : ViewModel(), KoinComponent {

    private val state = MutableLiveData<HomeViewState>()
    val stateLiveData: LiveData<HomeViewState> = state

    private val getCharacterListUseCase: GetCharacterListUseCase by inject()
    private val getHomeConfiguration: GetHomeConfigurationUseCase by inject()

    private var page = 1
    var isLastPage = false
    lateinit var config: HomeConfiguration

    sealed class HomeViewState {
        object LoadingCharacters: HomeViewState()
        class LoadedCharacters(val characters: List<Character>): HomeViewState()
        object ErrorLoadingCharacters: HomeViewState()
    }

    init {
        getConfiguration()
    }

    private fun getConfiguration() {
        viewModelScope.launch {
            config = getHomeConfiguration.execute()
        }
    }

    fun getCharacterList() {
        state.value = HomeViewState.LoadingCharacters

        viewModelScope.launch {
            getCharacterListUseCase.execute(page)
                .catch {
                    state.value = HomeViewState.ErrorLoadingCharacters
                }
                .collect {response ->
                    if(response.info.pages > page) {
                        page++
                    } else {
                        isLastPage = false
                    }
                    state.value = HomeViewState.LoadedCharacters(response.characters)
                }
        }
    }
}