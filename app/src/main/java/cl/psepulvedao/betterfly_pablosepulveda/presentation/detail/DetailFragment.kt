package cl.psepulvedao.betterfly_pablosepulveda.presentation.detail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import cl.psepulvedao.betterfly_pablosepulveda.R
import cl.psepulvedao.betterfly_pablosepulveda.databinding.DetailFragmentBinding
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.Character
import com.bumptech.glide.Glide
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
class DetailFragment : Fragment(R.layout.detail_fragment) {

    private val params: DetailFragmentArgs by navArgs()
    private val detailViewModel: DetailViewModel by viewModel()
    private lateinit var binding: DetailFragmentBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = DetailFragmentBinding.bind(view)

        setupViews()
        addObservers()
        loadCharacter(params.characterId)
    }

    private fun loadCharacter(characterId: Int) {
        detailViewModel.getCharacterDetail(characterId)
    }

    private fun setupViews() = with(binding) {
        btnRetry.setOnClickListener { loadCharacter(params.characterId)  }
    }

    private fun addObservers() {
        detailViewModel.stateLiveData.observe(viewLifecycleOwner) { handleViewState(it) }
    }

    private fun handleViewState(viewState: DetailViewModel.DetailViewState) {
        when(viewState) {
            DetailViewModel.DetailViewState.LoadingCharacter -> showLoading()
            is DetailViewModel.DetailViewState.LoadedCharacter -> showCharacter(viewState.character)
            DetailViewModel.DetailViewState.ErrorLoadingCharacter -> showError()
        }
    }

    private fun showError() = with(binding) {
        dataGroup.visibility = View.GONE
        progressBar.visibility = View.GONE
        btnRetry.visibility = View.VISIBLE
    }

    private fun showLoading() = with(binding) {
        dataGroup.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
        btnRetry.visibility = View.GONE
    }

    private fun showData() = with(binding) {
        dataGroup.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
        btnRetry.visibility = View.GONE
    }

    private fun showCharacter(character: Character) = with(binding) {
        showData()

        tvCharacterName.text = character.name
        tvStatus.text = character.status
        tvSpecies.text = character.species
        tvGender.text = character.gender

        Glide.with(requireContext())
            .load(character.image)
            .into(ivCharacter)
    }
}