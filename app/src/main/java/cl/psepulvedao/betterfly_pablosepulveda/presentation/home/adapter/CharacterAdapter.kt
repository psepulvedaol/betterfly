package cl.psepulvedao.betterfly_pablosepulveda.presentation.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import cl.psepulvedao.betterfly_pablosepulveda.databinding.ItemCharacterBinding
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.Character
import com.bumptech.glide.Glide

class CharacterAdapter(val listener: (character: Character) -> Unit): RecyclerView.Adapter<CharacterAdapter.ViewHolder>() {

    private val items = mutableListOf<Character>()

    fun addData(items: List<Character>) {
        val position = this.items.size
        if(this.items.containsAll(items).not()) {
            this.items.addAll(items)
        }
        notifyItemInserted(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemCharacterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(private val binding: ItemCharacterBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(character: Character) = with(binding) {
            tvCharacterName.text = character.name
            Glide.with(root)
                    .load(character.image)
                    .into(ivCharacter)

            root.setOnClickListener { listener(character) }
        }
    }
}