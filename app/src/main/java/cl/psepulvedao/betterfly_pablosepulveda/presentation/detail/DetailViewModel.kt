package cl.psepulvedao.betterfly_pablosepulveda.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.Character
import cl.psepulvedao.betterfly_pablosepulveda.domain.usecase.GetCharacterDetailUseCase
import cl.psepulvedao.betterfly_pablosepulveda.presentation.home.HomeViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class DetailViewModel : ViewModel(), KoinComponent {

    private val getCharacterDetailUseCase: GetCharacterDetailUseCase by inject()

    private val state = MutableLiveData<DetailViewState>()
    val stateLiveData: LiveData<DetailViewState> = state

    sealed class DetailViewState {
        object LoadingCharacter: DetailViewState()
        class LoadedCharacter(val character: Character): DetailViewState()
        object ErrorLoadingCharacter: DetailViewState()
    }

    fun getCharacterDetail(characterId: Int) {
        state.value = DetailViewState.LoadingCharacter

        viewModelScope.launch {
            getCharacterDetailUseCase.execute(characterId)
                .catch {
                    state.value = DetailViewState.ErrorLoadingCharacter
                }
                .collect {response ->
                    state.value = DetailViewState.LoadedCharacter(response)
                }
        }
    }
}