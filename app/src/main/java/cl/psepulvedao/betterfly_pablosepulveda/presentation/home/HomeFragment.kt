package cl.psepulvedao.betterfly_pablosepulveda.presentation.home

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import cl.psepulvedao.betterfly_pablosepulveda.R
import cl.psepulvedao.betterfly_pablosepulveda.databinding.HomeFragmentBinding
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.Character
import cl.psepulvedao.betterfly_pablosepulveda.presentation.common.PaginationScrollListener
import cl.psepulvedao.betterfly_pablosepulveda.presentation.home.adapter.CharacterAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.component.KoinApiExtension

private const val POSITIONS_BEFORE_LOAD_MORE = 8

@KoinApiExtension
class HomeFragment : Fragment(R.layout.home_fragment) {

    private val homeViewModel: HomeViewModel by viewModel()
    private lateinit var binding: HomeFragmentBinding
    private val adapter = CharacterAdapter { onCharacterSelected(it.id) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = HomeFragmentBinding.bind(view)

        setupViews()
        addObservers()

        if(adapter.itemCount == 0) {
            homeViewModel.getCharacterList()
        }
    }

    private fun setupViews() = with(binding) {
        val lManager =  GridLayoutManager(context, 2)
        val scrollListener = object: PaginationScrollListener(lManager, POSITIONS_BEFORE_LOAD_MORE) {
            override fun loadMoreItems() = homeViewModel.getCharacterList()

            override fun isLastPage(): Boolean = homeViewModel.isLastPage

            override fun isLoading(): Boolean =
                homeViewModel.stateLiveData.value == HomeViewModel.HomeViewState.LoadingCharacters
        }

        rvCharacters.layoutManager = lManager
        rvCharacters.adapter = this@HomeFragment.adapter
        rvCharacters.addOnScrollListener(scrollListener)

        btnRetry.setOnClickListener { homeViewModel.getCharacterList() }
    }

    private fun addObservers() {
        homeViewModel.stateLiveData.observe(viewLifecycleOwner) { handleViewState(it) }
    }

    private fun handleViewState(viewState: HomeViewModel.HomeViewState) {
        when(viewState) {
            is HomeViewModel.HomeViewState.LoadedCharacters -> showCharacters(viewState.characters)
            HomeViewModel.HomeViewState.ErrorLoadingCharacters -> showError()
            HomeViewModel.HomeViewState.LoadingCharacters -> showLoading()
        }
    }

    private fun showLoading() = with(binding) {
        if(adapter.itemCount == 0) {
            rvCharacters.visibility = View.GONE
            btnRetry.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        }
    }

    private fun showError() = with(binding) {
        if(adapter.itemCount == 0) {
            rvCharacters.visibility = View.GONE
            btnRetry.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }

    private fun showCharacterView() = with(binding) {
        rvCharacters.visibility = View.VISIBLE
        btnRetry.visibility = View.GONE
        progressBar.visibility = View.GONE
    }

    private fun showCharacters(character: List<Character>) {
        showCharacterView()
        adapter.addData(character)
    }

    private fun onCharacterSelected(id: Int) {
        if(homeViewModel.config.isDetailEnabled) {
            goToCharacterDetail(id)
        }
    }

    private fun goToCharacterDetail(id: Int) {
        val direction = HomeFragmentDirections.actionHomeFragmentToDetailFragment(id)
        findNavController().navigate(direction)
    }

}