package cl.psepulvedao.betterfly_pablosepulveda.domain.model

import com.google.gson.annotations.SerializedName

data class Info(
    val count: Int,
    val pages: Int
)