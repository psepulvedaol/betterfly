package cl.psepulvedao.betterfly_pablosepulveda.domain.repository

import cl.psepulvedao.betterfly_pablosepulveda.domain.model.HomeConfiguration

interface ConfigRepository {
    fun getHomeConfiguration(): HomeConfiguration
}