package cl.psepulvedao.betterfly_pablosepulveda.domain.di

import cl.psepulvedao.betterfly_pablosepulveda.domain.usecase.GetCharacterDetailUseCase
import cl.psepulvedao.betterfly_pablosepulveda.domain.usecase.GetCharacterListUseCase
import cl.psepulvedao.betterfly_pablosepulveda.domain.usecase.GetHomeConfigurationUseCase
import org.koin.core.component.KoinApiExtension
import org.koin.dsl.module

@OptIn(KoinApiExtension::class)
val useCaseModule = module {
    factory { GetCharacterListUseCase() }
    factory { GetCharacterDetailUseCase() }
    factory { GetHomeConfigurationUseCase() }
}