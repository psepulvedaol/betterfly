package cl.psepulvedao.betterfly_pablosepulveda.domain.repository

import cl.psepulvedao.betterfly_pablosepulveda.domain.model.Character
import cl.psepulvedao.betterfly_pablosepulveda.domain.model.CharacterList
import kotlinx.coroutines.flow.Flow

interface CharacterRepository {
    fun getAll(page: Int?): Flow<CharacterList>
    fun getDetail(id: Int): Flow<Character>
}