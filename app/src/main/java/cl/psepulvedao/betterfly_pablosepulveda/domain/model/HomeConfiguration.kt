package cl.psepulvedao.betterfly_pablosepulveda.domain.model

data class HomeConfiguration(val isDetailEnabled: Boolean)