package cl.psepulvedao.betterfly_pablosepulveda.domain.usecase

import cl.psepulvedao.betterfly_pablosepulveda.domain.repository.ConfigRepository
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class GetHomeConfigurationUseCase: KoinComponent {

    private val repository: ConfigRepository by inject()

    fun execute() = repository.getHomeConfiguration()
}