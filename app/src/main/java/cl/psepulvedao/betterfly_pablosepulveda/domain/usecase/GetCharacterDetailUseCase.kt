package cl.psepulvedao.betterfly_pablosepulveda.domain.usecase

import cl.psepulvedao.betterfly_pablosepulveda.domain.repository.CharacterRepository
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class GetCharacterDetailUseCase: KoinComponent {

    private val repository: CharacterRepository by inject()

    fun execute(characterId: Int) = repository.getDetail(characterId)
}