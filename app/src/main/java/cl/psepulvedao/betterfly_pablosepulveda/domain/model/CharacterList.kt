package cl.psepulvedao.betterfly_pablosepulveda.domain.model

data class CharacterList(
    val characters: List<Character>,
    val info: Info
)